<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();
    
    if (!empty($_COOKIE['save'])) {
        setcookie('save', '', 100000);
        $messages[] = 'Thank you, form was saved';
    }
    
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['data'] = !empty($_COOKIE['data_error']);
    $errors['pol'] = !empty($_COOKIE['pol_error']);
    $errors['konch'] = !empty($_COOKIE['konch_error']);
    $errors['sverh'] = !empty($_COOKIE['sverh_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    
    if ($errors['fio']) {
        setcookie('fio_error', '', 100000);
        $messages[] = '<div class="error">Write name</div>';
    }
    if ($errors['email']) {
        setcookie('email_error', '', 100000);
        $messages[] = '<div class="error">Write email</div>';
    }
    if ($errors['data']) {
        setcookie('data_error', '', 100000);
        $messages[] = '<div class="error">Write date</div>';
    }
    if ($errors['pol']) {
        setcookie('pol_error', '', 100000);
        $messages[] = '<div class="error">Write gender</div>';
    }
    if ($errors['konch']) {
        setcookie('konch_error', '', 100000);
        $messages[] = '<div class="error">Write limbs</div>';
    }
    if ($errors['sverh']) {
        setcookie('sverh_error', '', 100000);
        $messages[] = '<div class="error">Write abilities</div>';
    }
    if ($errors['bio']) {
        setcookie('bio_error', '', 100000);
        $messages[] = '<div class="error">Write biography</div>';
    }    
    
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];   
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['data'] = empty($_COOKIE['data_value']) ? '' : $_COOKIE['data_value'];
    $values['pol'] = empty($_COOKIE['pol_value']) ? '' : $_COOKIE['pol_value'];
    $values['konch'] = empty($_COOKIE['konch_value']) ? '' : $_COOKIE['konch_value'];
    $values['sverh'] = empty($_COOKIE['sverh_value']) ? '' : $_COOKIE['sverh_value'];
    $values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];   
    include('form.php');
}
else {
    $errors = FALSE;
    if (empty($_POST['fio'])) {
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['email'])) {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['data'])) {
        setcookie('data_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('data_value', $_POST['data'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['pol'])) {
        setcookie('pol_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('pol_value', $_POST['pol'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['konch'])) {
        setcookie('konch_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('konch_value', $_POST['konch'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['sverh'])) {
        setcookie('sverh_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('sverh_value', $_POST['sverh'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['bio'])) {
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('bio_value', $_POST['bio'], time() + 30 * 24 * 60 * 60);
    }

    
    if ($errors) {
        header('Location: index.php');
        exit();
    }
    else {
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('data_error', '', 100000);
        setcookie('pol_error', '', 100000);
        setcookie('konch_error', '', 100000);
        setcookie('sverh_error', '', 100000);
        setcookie('bio_error', '', 100000);
    }
    
    setcookie('save', '1');
    
    $user = 'u17088';
    $pass = '2421014';
    $db = new PDO('mysql:host=localhost;dbname=u17088', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    
    
    try {
        $stmt = $db->prepare("INSERT INTO application SET name = ?, email = ?, data = ?, gender = ?, limbs = ?, super = ?, bio = ?");
        
        
        $stmt->execute(array($_POST['fio'], $_POST['email'],$_POST['data'],$_POST['pol'],$_POST['konch'],
            $_POST['sverh'],$_POST['bio']));
        
        
        
    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    }
    
    
    header('Location: index.php');
}

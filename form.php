<!DOCTYPE html>
<html lang="ru">
<head>
<title>Fromm</title>
<meta charset="utf-8">
<link rel="stylesheet" href="style.css" >
</head>
<body>
<style>
body{
	background-color:#8FBC8F;
}
.main-form{
    width:60%;
    display: flex;
    background-color:#AFEEEE;
	justify-content:center;
	border-radius:20px;	
	margin-left:20%;
}
form{
    text-align: center;
}
.error {
  border: 2px solid red;
}
</style>

<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}


?>

<div class ="main-form">
			
<div class = "text-field">
<form action="" method="POST">
<label>
Put your name:<br />
<input type="text"
placeholder="name" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>" name="fio">
</label><br />
<label>
Put e-mail:<br />
<input type="email" name="email"
placeholder="e-mail" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>">
</label><br />
<label>
Put your data of born<br />
<input name="data" <?php if ($errors['data']) {print 'class="error"';} ?> value="<?php print $values['data']; ?>" type="date">
</label><br /><br>
Put your gender: <br>
<div <?php if ($errors['pol']) {print 'class="error"';} ?>>
<label><input type="radio" name="pol" <?php if ($errors['pol']and$values['pol']=="Genshina") {print 'checked="checked"';}?> value="Genshina"> Genshina </label>
<label><input type="radio" name="pol" <?php if ($errors['pol']and$values['pol']=="Mujic") {print 'checked="checked"';}?> value="Mujic"> Mujic </label><br><br>
</div>
<div <?php if ($errors['pol']) {print 'class="error"';} ?>>
Put kol-vo ends:<br>
<input type="radio" name="konch" <?php if ($errors['konch']and$values['konch']=="0") {print 'checked="checked"';}?>> 0
<label><input type="radio" name="konch" <?php if ($errors['konch']and$values['konch']=="2") {print 'checked="checked"';}?> value="2"> 2 </label>
<label><input type="radio" name="konch" <?php if ($errors['konch']and$values['konch']=="4") {print 'checked="checked"';}?> value="4"> 4 </label>
<label><input type="radio" name="konch" <?php if ($errors['konch']and$values['konch']=="5") {print 'checked="checked"';}?> value="5"> 5 </label>
<label><input type="radio" name="konch" <?php if ($errors['konch']and$values['konch']=="8") {print 'checked="checked"';}?> value="8"> 8 </label><br><br>
<label>
</div>

Put your sverhsposobnosti:<br>
<select name="sverh" multiple="multiple" <?php if ($errors['pol']) {print 'class="error"';} ?>>
<option value="Speed" <?php if ($errors['sverh']and$values['sverh']=="Speed") {print 'selected="selected"';}?>>Speed</option>
<option value="Power" <?php if ($errors['sverh']and$values['sverh']=="Power") {print 'selected="selected"';}?>>Power</option>
<option value="Three" <?php if ($errors['sverh']and$values['sverh']=="Three") {print 'selected="selected"';}?>>3 po diffuram</option>
<option value="Fly" <?php if ($errors['sverh']and$values['sverh']=="Fly") {print 'selected="selected"';}?>>Fly</option>
<option value="Nichego" <?php if ($errors['sverh']and$values['sverh']=="Nichego") {print 'selected="selected"';}?>>Nichego</option>
</select>
</label><br><br>
<label>

Biography
<br /><textarea id="main" name="bio"
placeholder="biography" <?php if ($errors['bio']) {print 'class="error"';} ?> ><?php print $values['bio']; ?></textarea>
</label><br><br>
<label>
<input type="checkbox" name="chika" >
S usloviami dogovora oznakomlen/a
</label><br><br>
<input type="submit"
value="Submit">
</form>
			</div>
		</div>
	</div>
	</body>
</html>
